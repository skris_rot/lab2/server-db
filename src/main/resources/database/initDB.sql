CREATE TABLE IF NOT EXISTS stud
(
id INT PRIMARY KEY,
firstName VARCHAR,
lastName VARCHAR,
description VARCHAR,
studentNumber int CHECK (studentNumber > 0)
);