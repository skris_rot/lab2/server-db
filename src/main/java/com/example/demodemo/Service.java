package com.example.demodemo;

import dto.PostStd;
import net.minidev.json.JSONArray;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Service {
    //  Database credentials
    static final String DB_URL = "jdbc:postgresql://db:5432/std";
    static final String USER = "postgres";
    static final String PASS = "123";

    public static List<Stud> selectFromStudTable() throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;

        String selectTableSQL = "SELECT id, firstname, lastname, studentnumber FROM public.stud;";
        List<Stud> s = new ArrayList<>();
        JSONArray std = new JSONArray();

        try {
            dbConnection = getDBConnection(DB_URL);
            statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);
            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                long id = rs.getLong("id");

                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                int studentnumber = rs.getInt("studentnumber");
                Stud person = new Stud(id, studentnumber,firstname,lastname);

                s.add(person);
                System.out.println(id+": " + " "+ firstname +" "+ lastname +" "+ studentnumber);
                ;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return s;
    }

    public static Stud createInStudTable(PostStd dto) throws SQLException {
        Connection dbConnection = null;
        Statement statement = null;

        String createInTableSQL = "INSERT INTO public.stud ( firstname, lastname, studentnumber)" +
                "VALUES('" + dto.getFirstName() +"', '" + dto.getLastName() +"',  " + dto.getStudentnumber() +")" +
                "RETURNING *;";
        Stud person = null;
        try {
            dbConnection = getDBConnection(DB_URL);
            statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(createInTableSQL);
            while (rs.next()) {
                Long id = rs.getLong("id");

                String firstname = rs.getString("firstname");
                String lastname = rs.getString("lastname");
                int studentnumber = rs.getInt("studentnumber");

                person = new Stud(id, studentnumber,firstname,lastname);

                ;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        finally {
            if (statement != null) {
                statement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return person;
    }


    private static Connection getDBConnection(String db) {
        Connection dbConnection = null;
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC Driver is not found. Include it in your library path ");
            e.printStackTrace();
        }
        try {
            dbConnection = DriverManager.getConnection(db, USER, PASS);
            return dbConnection;
        } catch (SQLException e) {
            System.out.println("Connection Failed");
            System.out.println(e.getMessage());
        }
        return dbConnection;
    }

}
