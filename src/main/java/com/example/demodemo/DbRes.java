package com.example.demodemo;


import dto.PostStd;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import serivce.UserService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@RestController
//@RequestMapping("/rest")


public class DbRes {

//
//    //  Database credentials
//    static final String DB_URL = "jdbc:postgresql://database:5432/std";
//    static final String DB = "jdbc:postgresql://database:5432";
//
//    static final String USER = "postgres";
//    static final String PASS = "123";

//    @RequestMapping("/")
    @RequestMapping(value = "/data")
    public @ResponseBody
    List<Stud> home() {
        List<Stud> s = new ArrayList<>();
        try {
            s =  Service.selectFromStudTable();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return s;
    }
    @PostMapping("/data/save")
    public @ResponseBody Stud putLineDataSave(
            @RequestBody PostStd dto
    )
    {
        Stud  s = null;
        try {
            s = Service.createInStudTable(dto);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }


        return s;
    }



}
