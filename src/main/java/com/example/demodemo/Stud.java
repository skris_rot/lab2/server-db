package com.example.demodemo;

public class Stud {
    private final long id;
    private final int studentnumber;
    private final String firstname;
    private final String lastname;

    public Stud(long id, int studentnumber, String firstname, String lastname) {
        this.id = id;
        this.studentnumber = studentnumber;
        this.firstname = firstname;
        this.lastname = lastname;
    }


    public long getId() {
        return id;
    }

    public int getStudentnumber() {
        return studentnumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }
}
