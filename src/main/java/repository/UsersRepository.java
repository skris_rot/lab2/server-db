package repository;


import models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
public interface UsersRepository extends JpaRepository<Users, Long> {

    List<Users> findAllByName(String name);//просто правильное название метода даст возможность
    //избежать запросов на SQL


    @Query(value = "select * from users;", nativeQuery = true)
        //если и этого мало - можно написать запрос на чистом SQL и все это будет работать
    List<Users> findWhereNameStartsFromSmith();
}
