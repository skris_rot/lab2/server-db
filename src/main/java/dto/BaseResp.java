package dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BaseResp<T>
{
    private T data;

    private Integer count;

    private String message;



    // STATIC CONSTRUCTORS

    @SuppressWarnings("unchecked")
    public static <T> BaseResp of(T data)
    {
        return new BaseResp(data, null, null);
    }

    @SuppressWarnings("unchecked")
    public static <T> BaseResp of(T data, Integer count)
    {
        return new BaseResp(data, count, null);
    }

    @SuppressWarnings("unchecked")
    public static <T> BaseResp of(T data, Integer count, String message)
    {
        return new BaseResp(data, count, message);
    }

    @SuppressWarnings("unchecked")
    public static <T> BaseResp of(T data, String message)
    {
        return new BaseResp(data, null, message);
    }

    @SuppressWarnings("unchecked")
    public static <T> BaseResp of(String message)
    {
        return new BaseResp(null, null, message);
    }

    public static BaseResp ofEmpty()
    {
        return BaseResp.of(null);
    }



    // CONSTRUCTORS

    public BaseResp(T data, Integer count, String message)
    {
        this.data = data;
        this.count = count;
        this.message = message;
    }
}
