CREATE DATABASE std;
\c std

CREATE TABLE stud ( id INT, firstName VARCHAR, lastName VARCHAR,  description VARCHAR,  studentNumber int CHECK (studentNumber > 0) );
